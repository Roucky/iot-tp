const express = require('express')
const app = express();
var fs = require('fs');
var bp = require('body-parser');

app.set('view engine', 'ejs')
app.use(bp.urlencoded({ extended: false }))
app.use(bp.json());
app.use(express.static('assets'));

app.get('/', function(req, res) {
    res.render('index')
})

app.get('/chart-:recordid', function(req, res) {
    res.render('chart', { recordid: req.params.recordid })
})

app.get('/chart', function(req, res) {
    res.render('chart', { recordid: 'first' })
})

app.get('/database', function(req, res) {
    fs.readFile('./database.json', 'utf-8', function(err, json) {
        res.send(json);
    });
})

app.listen(80, function() {

})