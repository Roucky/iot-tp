function generateSelect(selected) {
    $.get("./database", function(json) {
        json = JSON.parse(json)
        json = json.sort(function(a, b) {
            var x = a.nom;
            var y = b.nom;
            return ((x < y) ? -1 : ((x > y) ? 1 : 0));
        });
        json.forEach(element => {
            var isSelected = element.recordid == selected ? 'selected' : '';
            $('#station').append('<option ' + isSelected + ' value="' + element.recordid + '">' + element.nom + '</option>')
        });
        loadData($('#station').val())
    });
}

$('#station').on('change', function() {
    $(".chart_div").first().html('<div id="load" class="mt-5"><i class="fas fa-spinner fa-spin"></i></div>');
    loadData($(this).val());
});

var chart;
var first = true;
var datas = [];

function drawCurveTypes() {
    var data = new google.visualization.DataTable();
    data.addColumn('date', 'Heure');
    data.addColumn('number', 'Velos dispo');
    data.addColumn('number', 'Places dispo');

    data.addRows(datas);

    var options = {
        series: {
            1: {
                curveType: 'function'
            }
        }
    };

    chart = new google.visualization.LineChart($(".chart_div")[0]);
    chart.draw(data, options);
}

function loadData(id) {
    datas = [];
    $.get("./database", function(json) {
        json = JSON.parse(json)
        json.forEach(record => {
            if (record.recordid == id) {
                record.datas.forEach(element => {
                    var b = [];
                    b.push(new Date(element.datetime));
                    b.push(element.nbvelosdispo);
                    b.push(element.nbplacesdispo);
                    datas.push(b);
                });
            }
        });
    });
    setTimeout(function() {
        drawCurveTypes()
    }, 500);
}

google.load('visualization', '1', {
    packages: ['corechart', 'line']
});