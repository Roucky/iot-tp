var plage_selected = 0;
var nextHours = [];

function show_horaires() {
    $('#horaires_data').empty();
    nextHours = getNextHours(10);
    var startDatetime = new Date(nextHours[0]);
    var endDatetime = new Date(nextHours[nextHours.length - 1]);
    $('#startTime').html(startDatetime.getHours() + 'h');
    $('#endTime').html(endDatetime.getHours() + 'h');
    nextHours.forEach((element, i) => {
        if (i < nextHours.length - 1) {
            element = new Date(element);
            nextElement = new Date(nextHours[i + 1]);
            $('#horaires_data').append('<div class="time" onclick="plage(' + i + ',\'' + element.getHours() + 'h - ' + nextElement.getHours() + 'h' + '\');"></div>')
        }
    });
    plage(0, startDatetime.getHours() + 'h - ' + endDatetime.getHours() + 'h');
}

function plage(i, data) {
    plage_selected = i;
    $('#plage').html(data);
    $('.time').each(function(n) {
        if (n != i) {
            $(this).removeClass("time_selected");
        } else {
            $(this).addClass("time_selected");
        }
    });
}

function frequenceForDateTime() {
    var datetime = new Date(nextHours[plage_selected]);
    datetime.setUTCMinutes(0);
    datetime.setUTCSeconds(0);
    datetime.setUTCMilliseconds(0);

    var recordsManaged = [];
    var recordsForDatetime = [];
    $.get("./database", function(json) {
        json = JSON.parse(json)
        json.forEach((element, i) => {
            var recordDatas = [];
            element.datas.forEach((datas, i) => {
                datas.datetime = new Date(datas.datetime);
                datas.datetime.setUTCMinutes(0);
                datas.datetime.setUTCSeconds(0);
                datas.datetime.setUTCMilliseconds(0);
                if (datas.datetime.getHours() == datetime.getHours()) {
                    recordDatas.push(datas);
                }
            });
            recordsForDatetime.push({ 'recordid': element.recordid, 'datas': recordDatas });
        });
        recordsForDatetime.forEach((elements, i) => {
            var nbVeloDispo = 0;
            var nbPlacesTotal = 0;
            var nb = Number.isInteger(elements.datas.length) ? elements.datas.length : '0';
            elements.datas.forEach(element => {
                nbVeloDispo += element.nbvelosdispo;
                nbPlacesTotal += (element.nbvelosdispo + element.nbplacesdispo);
            });
            elements.avgVeloDispo = division(nbVeloDispo, nb);
            elements.nbPlaces = division(nbPlacesTotal, nb);
            elements.moment = datetime;
            elements.score = division(elements.avgVeloDispo, elements.nbPlaces);
            delete elements.datas;
        });
    });
    return recordsForDatetime;
}

function division(a, b) {
    if (b === 0 || isNaN(b)) {
        return 0;
    } else {
        return (a / b).toFixed(0);
    }
}

var client = new Paho.MQTT.Client('broker.mqttdashboard.com', 8000, 'clientId-' + Math.floor(Math.random() * 1000000))

const TOPIC = 'ade5587f-9d2e-4c40-a4a8-3e3dc7759aff'
const DISTANCE_MAX = 100;

var records;
var show_records = [];
var records_beststation = [];

var options = {

    timeout: 3,

    onSuccess: function() {
        onConnect()
    },

    onFailure: function(message) {
        console.log("Connection failed: " + message.errorMessage);
    }

};
client.connect(options)

function onConnect() {
    client.subscribe(TOPIC)
}

client.onMessageArrived = function(message) {
    var data = JSON.parse(message.payloadString);
    records = data.records;
    refreshDatetime(data.refreshDatetime);
    refreshCards();
};

function resetRecordBestStation() {
    records_beststation = [];
    records.forEach(record => {
        delete record.fields.avgVeloDispo;
        delete record.fields.nbPlaces;
        delete record.fields.moment;
    });
}

$('#searchButton').on('click', function() {
    resetRecordBestStation();
    refreshCards();
});

$('#geolocalize').on('click', function() {
    $('#geolocalize').tooltip('hide')
    localize(false);
});

$('#lucky').on('click', function() {
    $('#lucky').tooltip('hide')
    localize(true)
});

$('#luckySubmit').on('click', function() {
    records_beststation = frequenceForDateTime();
    $('#horaires').fadeOut(500);
    setTimeout(function() {
        refreshCards();
        $('#cards').fadeIn(500);
    }, 500);
});

function localize(luckyCallback) {
    var beforeContent = $('#geolocalize').html();
    $('#geolocalize').html('<i class="fas fa-spinner fa-spin"></i>');
    navigator.geolocation.getCurrentPosition(function(position) {
        $('#geolocalize').html(beforeContent);
        $('#search').val(position.coords.latitude + ',' + position.coords.longitude);
        resetRecordBestStation();
        if (luckyCallback) {
            $('#cards').fadeOut(500);
            setTimeout(function() {
                show_horaires();
                $('#horaires').fadeIn(500);
            }, 500);
        } else {
            refreshCards();
        }
    }, function() {
        alert('Localisation impossible');
        $('#geolocalize').html(beforeContent);
    }, {
        enableHighAccuracy: true
    });
}

function refreshCards() {
    $('#cards').empty();
    show_records = [];
    // traitement de chaque station
    records.forEach(record => {
        manageRecord(record, checkCoords(getSearch()));
    });

    // tri des stations
    sortData();

    // affichage des stations
    show_records.forEach(record => {
        $('#cards').append(formatCard(record));
    });
}

function sortData() {
    if (records_beststation.length > 0) {
        show_records.forEach((element, i) => {
            records_beststation.forEach(r => {
                if (r.recordid == element.recordid) {
                    element.fields.score = r.score - element.fields.distance;
                    element.fields.avgVeloDispo = r.avgVeloDispo;
                    element.fields.nbPlaces = r.nbPlaces;
                    element.fields.moment = r.moment;
                }
            });
        });

        show_records = jQuery.grep(show_records, function(value) {
            if (value.fields.etat == "EN SERVICE") {
                return true;
            }
        });

        show_records = show_records.sort(function(a, b) {
            var x = a.fields.score;
            var y = b.fields.score;
            return ((x > y) ? -1 : ((x < y) ? 1 : 0));
        });
    } else {
        show_records = show_records.sort(function(a, b) {
            var x = a.fields.nom;
            var y = b.fields.nom;
            if (checkCoords(getSearch())) {
                x = a.fields.distance;
                y = b.fields.distance;
            }
            return ((x < y) ? -1 : ((x > y) ? 1 : 0));
        });
    }

}

var formatHtmlRender =
    '<div class="card">' +
    '<div class="card-header align-middle" style="height: 65px; display: flex; align-items: center;">' +
    '<div>{etat}</div>' +
    '<div class="card-title p-0 m-0 text-muted" style="font-weight: bold; font-size: 18px;">{nom}</div>' +
    '</div>' +
    '<div class="card-body text-muted">' +
    '<h6 class="card-subtitle mb-2 text-uppercase"><i class="fas fa-map-marker-alt mr-2"></i>{adresse}, {commune}</h6>' +
    '<h6 class="my-2">{distance}</h6>' +
    '<h6 class="my-2"><i class="fas fa-align-right mr-2" style="transform: rotate(90deg)"></i><a href="./chart-{recordid}" class="text-muted">voir l\'affluence</a></h6>' +
    '<h6 class="my-2">{avg}</h6>' +
    '</div>' +
    '<div class="card-footer text-center text-muted p-0" style="background: rgba(240,240,240,0.3)">' +
    'Actuellement' +
    '</div>' +
    '<div class="card-footer text-center">' +
    '<div class="row">' +
    '<div class="col-6 text-{color-velo}"><b>{nbvelosdispo}</b><br>vélos dispo.</div>' +
    '<div class="col-6 text-{color-place}"><b>{nbplacesdispo}</b><br>places dispo.</div>' +
    '</div>' +
    '</div>' +
    '</div>';

function formatCard(record) {
    var format = formatHtmlRender;
    var fields = record.fields;
    var nbTotal = fields.nbvelosdispo + fields.nbplacesdispo;
    format = format.replace('{recordid}', record.recordid);
    format = format.replace('{nom}', fields.nom);
    format = format.replace('{adresse}', fields.adresse);
    format = format.replace('{commune}', fields.commune);
    format = format.replace('{nbvelosdispo}', fields.nbvelosdispo);
    format = format.replace('{nbplacesdispo}', fields.nbplacesdispo);
    format = format.replace('{etat}', getIconEtat(fields.etat));

    format = format.replace('{color-velo}', getColor(fields.nbvelosdispo, nbTotal, fields.etat));
    format = format.replace('{color-place}', getColor(fields.nbplacesdispo, nbTotal, fields.etat));

    if (fields.distance != 'none') {
        var distance = fields.distance;
        if (distance < 1) {
            distance = (distance * 1000) + 'm';
        } else {
            distance += 'km';
        }
        var destination = fields.localisation[0] + ',' + fields.localisation[1];
        var moment = new Date(fields.moment);
        format = format.replace('{distance}', '<i class="fas fa-map-marked-alt mr-2"></i>située à ' + distance + ' <a target="_blank" href="' + getGoogleMapRoutesLink(getSearch(), destination) + '" class="text-dark"><i class="fas fa-arrow-alt-circle-right"></i></a>');
        if (fields.avgVeloDispo) {
            format = format.replace('{avg}', '<i class="fas fa-clock mr-2"></i>en moyenne à ' + moment.getHours() + 'h, <b>' + fields.avgVeloDispo + '</b> vélos sont disponibles sur <b>' + fields.nbPlaces + '</b>');
        } else {
            format = format.replace('{avg}', '');
        }
    } else {
        format = format.replace('{distance}', '');
        format = format.replace('{avg}', '');
    }
    return format;
}

function getIconEtat(etat) {
    var color = 'red';
    if (etat == "EN SERVICE") {
        color = 'green';
    }
    return '<svg class="bd-placeholder-img rounded-circle mr-2 mt-n1" width="20" height="20" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img"><rect fill="' + color + '" width="100%" height="100%"></rect></svg>';
}

function getColor(nb, nbTotal, etat) {
    if (etat == "HORS SERVICE") {
        return 'danger';
    }
    var percent = nb / nbTotal;
    if (percent <= 0.33) {
        return 'danger';
    } else if (percent <= 0.67) {
        return 'warning';
    }
    return 'success';
}

function manageRecord(record, localisation) {
    var search = getSearch();
    var fields = record.fields;
    var show = false;
    fields.distance = 'none';
    if (localisation) {
        var latlon = search.split(',');
        fields.distance = distance(latlon[0], latlon[1], fields.localisation[0], fields.localisation[1]);
    }
    if (search == '') {
        show = true;
    } else {
        if (checkCoords(search)) {
            if (fields.distance < DISTANCE_MAX) {
                show = true;
            }
        } else {
            var fieldsSearch = [fields.nom, fields.adresse, fields.commune];
            fieldsSearch.forEach(element => {
                if (element.toLowerCase().includes(search.toLowerCase())) {
                    show = true;
                }
            });
        }
    }
    if (show) {
        show_records.push(record);
    }
}

function getSearch() {
    return $('#search').val();
}

function refreshDatetime(datetime) {
    $('#refresh-datetime').html('Dernière actualisation : <b>' + formatDatetime(datetime) + '</b>');
}

function formatDatetime(datetime) {
    var datetime = new Date(datetime);
    var hours = '0' + datetime.getHours();
    var minutes = '0' + datetime.getMinutes();
    return hours.substr(-2) + 'h' + minutes.substr(-2);
}

function distance(lat1, lon1, lat2, lon2) {
    var R = 6371;
    var dLat = toRad(lat2 - lat1);
    var dLon = toRad(lon2 - lon1);
    var lat1 = toRad(lat1);
    var lat2 = toRad(lat2);

    var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
        Math.sin(dLon / 2) * Math.sin(dLon / 2) * Math.cos(lat1) * Math.cos(lat2);
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    var d = R * c;
    return d.toFixed(2);
}

function toRad(Value) {
    return Value * Math.PI / 180;
}

function checkCoords(coords) {
    check = /^([-+]?)([\d]{1,2})(((\.)(\d+)(,)))(\s*)(([-+]?)([\d]{1,3})((\.)(\d+))?)$/;
    return check.test(coords);
}

function getGoogleMapRoutesLink(coords1, coords2) {
    return "https://www.google.com/maps/dir/'" + coords1 + "'/'" + coords2 + "'/data=!4m10!4m9!1m3!2m2!1d2.8409856!2d50.3447552!1m3!2m2!1d3.057256!2d50.62925!3e2";
}

function getNextHours(nb) {
    var hours = [];
    for (var i = 0; i < nb; i++) {
        var d = new Date();
        d.setHours(d.getHours() + i);
        hours.push(d);
    }
    return hours;
}