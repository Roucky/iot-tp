**GOOGLE SLIDE**

https://docs.google.com/presentation/d/1W2sSr0eNy-rPaamZ_8oiCwI6qWx6HnhXLvCYt4xl3b0/edit?usp=sharing

Les questions se trouvent sur le slide.

**Comment lancer les projets ?**

1.  Avoir Node.js d'installé (Nous sommes en version 12)
2.  Executer la commande 'node index.js' dans le dossier mqtt
3.  Executer la commande 'node index.js' dans le dossier web
4.  Se rendre sur localhost

**A quoi servent les deux projets ?**

*  Le projet 'mqtt' permet d'effectuer l'appel API toutes les 2 minutes pour récupérer les données des stations de vélos, et de publier le résultat JSON dans un channel MQTT.
*  Il permet aussi d'insérer les données toutes les 10 minutes dans un fichier JSON (database) pour réaliser les statistiques.
*  
*  Le projet 'web' est l'application web utilisant les données présentes dans le channel MQTT, de les traiter et de les afficher.
*  Il utilise aussi le fichier JSON database pour afficher les statistiques et conseiller les utilisateurs.