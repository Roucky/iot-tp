var mqtt = require('mqtt');
var CronJob = require('cron').CronJob;
const https = require('https');
var fs = require('fs');

var client = mqtt.connect('mqtt://broker.mqttdashboard.com');
const TOPIC = 'ade5587f-9d2e-4c40-a4a8-3e3dc7759aff';
const URL_API = 'https://opendata.lillemetropole.fr/api';

var job = new CronJob('*/2 * * * *', function() {
    getData();
}, null, true, 'America/Los_Angeles');
job.start();

function publishData(data) {
    client.subscribe(TOPIC, function(err) {
        if (!err) {
            client.publish(TOPIC, data, {
                'retain': true
            });
        }
    });
}

function getData() {

    var facets = ['libelle', 'nom', 'commune', 'etat', 'type', 'etatconnexion', 'nbvelosdispo', 'nbplacesdispo'];
    https.get(URL_API + '/records/1.0/search/?apikey=6f93d78c085ee3d3bd4be348db9af8d018f7f1ebd276bf2f68618dfd&rows=244&dataset=vlille-realtime&facet=' + facets.join('&facet='), (resp) => {
        let data = '';

        resp.on('data', (chunk) => {
            data += chunk;
        });

        resp.on('end', () => {
            data = JSON.parse(data);
            data.refreshDatetime = new Date();
            if (data.refreshDatetime.getMinutes() % 10 == 0) {
                saveData(JSON.stringify(data));
            }
            publishData(JSON.stringify(data));
        });

    }).on("error", (err) => {
        console.log("Error: " + err.message);
    });
}

function saveData(data) {
    data = JSON.parse(data);
    var newJson = [];
    fs.readFile('../web/database.json', 'utf-8', function(err, json) {
        if (json != '') {
            json = JSON.parse(json);
            json.forEach(element => {
                data.records.forEach(record => {
                    if (record.recordid == element.recordid) {
                        element.nom = record.fields.nom;
                        element.datas.push({
                            "datetime": data.refreshDatetime,
                            "nbvelosdispo": record.fields.nbvelosdispo,
                            "nbplacesdispo": record.fields.nbplacesdispo
                        });
                    }
                })
                newJson.push(element);
            });
        } else {
            data.records.forEach(record => {
                newJson.push({
                    "recordid": record.recordid,
                    "nom": record.fields.nom,
                    "datas": [{
                        "datetime": data.refreshDatetime,
                        "nbvelosdispo": record.fields.nbvelosdispo,
                        "nbplacesdispo": record.fields.nbplacesdispo
                    }]
                });
            })
        }
        fs.writeFile('../web/database.json', JSON.stringify(newJson), function(err) {
            console.log(err);
        });
    });
}